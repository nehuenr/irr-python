import matplotlib.pyplot as plt
import sys


def calculateNPV (irr, payment, periods):

	npv = 0

	for t in range (1, periods + 1):

		npv += payment/((1 + irr)**t)

	return npv 


def calculateNFV (irr, payment, periods):		# Net Future Value

	nfv = 0

	for t in range (0, periods):

		nfv += payment * ((1 + irr)**t)

	return nfv 



def calibrateChange (change, diff, pos_diff):

	sign = (diff) / abs (diff)

	if pos_diff == True and sign < 0:

		change /= 10

		pos_diff = False

	elif pos_diff == False and sign > 0:

		change /= 10

		pos_diff = True

	return change, sign, pos_diff



def calculateImplicitIRR (npv, payment, periods):

	irr = 0.1

	change = irr

	irr_array = []
	irr_array.append (irr)

	expected_npv = calculateNPV (irr, payment, periods)
	expected_nfv = calculateNFV (irr, payment, periods)

	positive_diff = True

	epsilon = 0.01


	while abs (expected_npv - npv) > epsilon:

		change, sign, positive_diff = calibrateChange (change, expected_npv - npv, positive_diff)

		irr += change * sign 

		irr_array.append (irr)

		expected_npv = calculateNPV (irr, payment, periods)
		expected_nfv = calculateNFV (irr, payment, periods)



	difference = expected_npv - npv

	return irr, irr_array, epsilon, difference, expected_nfv


'''
npv = 28000+3200			# the price today
payment = 39000/6			# payment for every period
periods = 6					# how many periods (months)
year_periods = 12			# how many periods are there in a year
'''

'''
npv = 79690					# the price today
payment = 7667				# payment for every period
periods = 12				# how many periods (months)
year_periods = 12			# how many periods are there in a year
'''

'''
npv = 79690					# the price today
payment = 4996				# payment for every period
periods = 24				# how many periods (months)
year_periods = 12			# how many periods are there in a year
'''

#'''
npv = 79690					# the price today
payment = 4299				# payment for every period
periods = 30				# how many periods (months)
year_periods = 12			# how many periods are there in a year
#'''


irr, irr_array, epsilon, error, nfv = calculateImplicitIRR (npv, payment, periods)



print ("Period Interest = " + str (round (irr * 100, 2)) + "%")
print ("Transaction Interest = " + str (round (((1 + irr)**(periods) - 1) * 100, 2)) + "%")
print ("Annual Interest = " + str (round (((1 + irr)**(periods*(year_periods/periods)) - 1) * 100, 2)) + "%")
print ("Today Price = " + str (npv))
print ("Current Price Tag = " + str (round (payment * periods, 2)))
print ("Future Price Tag = " + str (round (nfv, 2)))
#print ("Annual Interest = " + str (round ((((nfv/npv)**(year_periods/periods)) - 1) * 100, 2)) + "%")
#print ("Epsilon = " + str (epsilon))
#print ("Error = " + str (round (error, 6)))

#plt.plot (range (len (irr_array)), irr_array)			# with interest
#plt.show ()